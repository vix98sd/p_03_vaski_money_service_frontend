import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.css';
import ImportExportPage from './components/ImportExportPage';
import NewTransactionPage from './components/NewTransactionPage';
import NewWalletPage from './components/NewWalletPage';
import WalletHomePage from './components/WalletHomePage';
import WalletInfoPage from './components/WalletInfoPage';

function App() {
  return (
    <Router>
      <div className="App card">
        <Routes>
          <Route path='/' element={<WalletHomePage />} />
          <Route path='/info/:wallet_id' element={<WalletInfoPage />} />
          <Route path='/new_wallet' element={<NewWalletPage />} />
          <Route path='/new_transaction' element={<NewTransactionPage />} />
          <Route path='/import_export' element={<ImportExportPage />} />
          <Route path='*' element={<div>404 Not found</div>} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
