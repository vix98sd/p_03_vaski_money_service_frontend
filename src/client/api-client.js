import axios from "axios";
const protocol = process.env.REACT_APP_PROTOCOL ? process.env.REACT_APP_PROTOCOL : 'http';
const domain = process.env.REACT_APP_DOMAIN ? process.env.REACT_APP_DOMAIN : 'localhost';
const port = process.env.REACT_APP_PORT ? process.env.REACT_APP_PORT : '30101';
const apiVersion = "v1"
const endpoint = `${protocol}://${domain}:${port}/api/${apiVersion}`

export default {
    initWallets(setWallets){
        (async() => axios.get(`${endpoint}/wallets/init`))()
    },
    getAllWallets(setWallets) {
        (async() => axios.get(`${endpoint}/wallets`)
            .then(response => setWallets(response.data.wallets))
        )()
    },
    getWalletById(walletId, setWallet, setTransactions){
        (async() => axios.get(`${endpoint}/wallets/${walletId}`)
            .then(response => {
                setWallet(response.data.wallet)
                setTransactions(response.data.walletTransactions)
            })
        )()
    },
    getAllTransactionTemplates(setTransactionTemplates) {
        (async() => axios.get(`${endpoint}/transactions/template`)
            .then(response => setTransactionTemplates(response.data))
        )()
    },
    postNewWallet(wallet){
        axios.post(`${endpoint}/wallets`, wallet)
        .catch(error => { 
            if(error.response.status === 409)
                alert(`wallet with name '${wallet.name}' already exists`)
        });
    },
    postNewTransaction(transaction){
        axios.post(`${endpoint}/transactions/${
            transaction.category === 'EARNING' ? 'earning' : 
            transaction.category === 'INTERNAL' ? 'internal/' + transaction.sendingWalletId + '/' + transaction.receivingWalletId :
            transaction.category === 'SPENDING' ? 'spending/' + transaction.sendingWalletId : ''
        }`, transaction);
    },
    postNewTransactionWithTemplate(transaction){
        axios.post(`${endpoint}/transactions/template/${
            transaction.category === 'EARNING' ? 'earning' : 
            transaction.category === 'INTERNAL' ? 'internal/' + transaction.sendingWalletId + '/' + transaction.receivingWalletId :
            transaction.category === 'SPENDING' ? 'spending/' + transaction.sendingWalletId : ''
        }`, transaction);
    },
    deleteWalletById(walletId){
        axios.delete(`${endpoint}/wallets/${walletId}`)
    },
    deleteTransactionById(transactionId){
        axios.delete(`${endpoint}/transactions/${transactionId}`)
    },
    exportData(setData){
        (async() => axios.get(`${endpoint}/export`)
            .then(response => {
                setData(JSON.stringify(response.data, undefined, 4))
            })
        )()
    },
    importData(data){
        axios.post(`${endpoint}/import`, JSON.parse(data));
    }
}
