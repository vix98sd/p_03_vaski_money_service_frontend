import React from 'react'
import { Link } from 'react-router-dom'

const LinkButton = (props) => {
  return (
    <Link to={`${props.to}`} className="btn btn-primary m-1">{props.text}</Link>
  )
}

export default LinkButton