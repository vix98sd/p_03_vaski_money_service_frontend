import React from 'react'

const ButtonDelete = (props) => {
  return (
    <button onClick={props.onClick} className="btn btn-danger m-1">{props.text}</button>
  )
}

export default ButtonDelete