import React from 'react'
import LinkButton from './LinkButton'

const ButtonBack = () => {
  return (
    <LinkButton to='/' text='Back' />
  )
}

export default ButtonBack