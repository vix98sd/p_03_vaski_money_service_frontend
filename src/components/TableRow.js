import React from 'react'

const TableRow = (props) => {

    const data = props.data ? props.data : []
    const rowMarkup = data.length > 0 ? (data.map((d, index) => <td key={index}>{d}</td>)) : (<td>/</td>);

    return (
        <tr>
            {rowMarkup}
        </tr>
    ) 
}

export default TableRow