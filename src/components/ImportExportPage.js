import React, { useEffect, useState } from 'react'
import apiClient from '../client/api-client'
import ButtonBack from './ButtonBack'

const ImportExportPage = () => {
    const [action, setAction] = useState('import')
    const [data, setData] = useState('')

    useEffect(() => {
        if(action === 'import'){
            setData('')
        }
    }, [action])

    const submitHandler = () => {
        if(action === 'export'){
            apiClient.exportData(setData);
        }else if(action === 'import'){
            apiClient.importData(data);
        }
    };

    const clearData = (e) => {
        try {
            const newData = JSON.stringify(JSON.parse(data), undefined, 4);
            setData(newData);
        } catch (error) {
            alert("JSON is not correct!")
        }
    }

    return (
        <div>
            <h5 className="card-header">Import / Export data</h5>
            <ButtonBack />
            <div className="form-floating mt-1">
                <select defaultValue={action} onChange={e => setAction(e.target.value)} className="form-select" id="actionSelector">
                    <option value='import'>Import</option>
                    <option value='export'>Export</option>
                </select>
                <label htmlFor="actionSelector">Select action</label>
            </div>
            <div className="form-floating mt-1">
                <textarea value={data} onChange={e => setData(e.target.value)} className="form-control" style={{height: '700px'}}></textarea>
                <label htmlFor="dataTextArea">Data</label>
            </div>
            <button onClick={submitHandler} type="button" className="btn btn-primary m-1">{action.charAt(0).toUpperCase() + action.slice(1)} data</button>
            <button onClick={e => clearData(e)} type="button" className="btn btn-primary m-1">Clear data</button>
        </div>
    )
}

export default ImportExportPage