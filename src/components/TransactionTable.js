import React from 'react'
import TableHeadRow from './TableHeadRow'
import TransactionTableRow from './TransactionTableRow'

const TransactionTable = (props) => {
  const transactions = props.transactions ? props.transactions : []
  const transactionListMarkup = transactions.length > 0 ? (
    transactions.map(transaction => (<TransactionTableRow key={transaction.id} transaction={transaction} />))
  ) : (<TransactionTableRow />)

  return (
    <table className="table table-striped table-hover">
      <thead>
        <TableHeadRow data={["Id", "Amount", "Reason", "Category", "Date", "Time", "Type"]} />
      </thead>
      <tbody>
          {transactionListMarkup}
      </tbody>
    </table>
  )
}

export default TransactionTable