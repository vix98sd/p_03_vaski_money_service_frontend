import React from 'react'

const TableHeadRow = (props) => {

    const data = props.data ? props.data : []
    const rowMarkup = data.length > 0 ? (data.map((d, index) => <th key={index}>{d}</th>)) : (<th>/</th>);

    return (
        <tr>
            {rowMarkup}
        </tr>
    ) 
}

export default TableHeadRow