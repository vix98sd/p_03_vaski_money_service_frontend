import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import apiClient from '../client/api-client'
import ButtonBack from './ButtonBack'

const NewTransactionPage = () => {
  const navigate = useNavigate();
  const today = new Date()

  const [wallets, setWallets] = useState([])
  const [transactionTemplates, setTransactionTemplates] = useState([])
  const [sendingWalletId, setSendingWalletId] = useState(3)
  const [receivingWalletId, setReceivingWalletId] = useState(3)
  const [amount, setAmount] = useState(1000)
  const [reason, setReason] = useState('Salary')
  const [saveAsTemplate, setSaveAsTemplate] = useState(`no`)
  const [transactionCategory, setTransactionCategory] = useState('spending')
  const [year, setYear] = useState(today.getFullYear())
  const [month, setMonth] = useState(today.getMonth() + 1)
  const [day, setDay] = useState(today.getDate())
  const [hour, setHour] = useState(today.getHours())
  const [minute, setMinute] = useState(today.getMinutes());

  const [isDisabledWallet1, setIsDisabledWallet1] = useState(false)
  const [isDisabledWallet2, setIsDisabledWallet2] = useState(true)
  const [isDisabledReasonField, setIsDisabledReasonField] = useState(false)
  const [isDisabledSubmitButton, setIsDisabledSubmitButton] = useState(false)


  useEffect(() => {
    apiClient.getAllWallets(setWallets);
    apiClient.getAllTransactionTemplates(setTransactionTemplates);
  }, [])

  useEffect(() => {if(wallets.length > 0) {setSendingWalletId(wallets[0].id); setReceivingWalletId(wallets[0].id)}}, [wallets])

  useEffect(() => {
    if(transactionCategory === 'earning'){
      setIsDisabledWallet1(true);
      setIsDisabledWallet2(true);
      setIsDisabledReasonField(false);
    } else
    if(transactionCategory === 'internal'){
      setIsDisabledWallet1(false);
      setIsDisabledWallet2(false);
      setIsDisabledReasonField(true);
    } else
    if(transactionCategory === 'spending'){
      setIsDisabledWallet1(false);
      setIsDisabledWallet2(true);
      setIsDisabledReasonField(false);
    }
  }, [transactionCategory])


  const transactionTemplatesMarkup = transactionTemplates.length > 0 ? (transactionTemplates.map((transactionTemplate, index) =>
    (<option key={transactionTemplate.id} value={index}>{transactionTemplate.transaction.associatedWallet.name} - {transactionTemplate.transaction.amount} - {transactionTemplate.transaction.reason}</option>))) : null;

  const walletsMarkup = wallets.length > 0 ? (wallets.map(wallet =>
    (<option key={wallet.id} value={wallet.id}>{wallet.name}</option>))) : null;

  const transactionCategoryMarkup = (['earning', 'internal', 'spending'].map(transaction =>
    (<option key={transaction} value={transaction}>{transaction.charAt(0).toUpperCase() + transaction.slice(1)} transaction</option>)));

  const addLeadingZero = (number) => `${number.toString().length === 1 ? '0' : ''}${number}`

  const transactionTemplateChangeHandler = (index) => {
    setSendingWalletId(transactionTemplates[index].transaction.associatedWallet.id);
    setTransactionCategory(transactionTemplates[index].transaction.category.toLowerCase());
    setReason(transactionTemplates[index].transaction.reason);
    setAmount(transactionTemplates[index].transaction.amount);
  }

  const submitHandler = () => {
    const transaction = {
      sendingWalletId: sendingWalletId,
      receivingWalletId: receivingWalletId,
      amount: amount,
      date: year + '-' + addLeadingZero(month)  + '-' + addLeadingZero(day),
      time: addLeadingZero(hour) + ':' + addLeadingZero(minute),
      reason: reason,
      category: transactionCategory.toUpperCase()
    }
    setIsDisabledSubmitButton(true)
    if(saveAsTemplate === `no`)
      apiClient.postNewTransaction(transaction)
    else
      apiClient.postNewTransactionWithTemplate(transaction)
    
    setTimeout(() => navigate('/'), 250)
  }

  return (
    <div>
      <h5 className="card-header">New transaction</h5>
      <ButtonBack />
      <div className="row mt-1 g-2">
        <div className="col-md">
          <div className="form-floating">
            <select defaultValue={`none`} onChange={e => transactionTemplateChangeHandler(e.target.value)} className="form-select" id="templateSelector" aria-label="Floating label select example">
              <option value={`none`} disabled hidden>Select template</option>
              {transactionTemplatesMarkup}
            </select>
            <label htmlFor="templateSelector">Select template for transaction</label>
          </div>
        </div>
      </div>
      <div className="row mt-1 g-2">
        <div className="col-md">
          <div className="form-floating">
            <select value={sendingWalletId} onChange={e => setSendingWalletId(e.target.value)} disabled={isDisabledWallet1} className="form-select" id="sendingWalletSelector" aria-label="Floating label select example">
              {walletsMarkup}
            </select>
            <label htmlFor="sendingWalletSelector">Select sending wallet</label>
          </div>
        </div>
        <div className="col-md">
          <div className="form-floating">
            <select defaultValue={receivingWalletId} onChange={e => setReceivingWalletId(e.target.value)} disabled={isDisabledWallet2} className="form-select" id="receivingWalletSelector" aria-label="Floating label select example">
              {walletsMarkup}
            </select>
            <label htmlFor="receivingWalletSelector">Select receiving wallet</label>
          </div>
        </div>
        <div className="col-md">
          <div className="form-floating">
            <select value={transactionCategory} onChange={e => setTransactionCategory(e.target.value)} className="form-select" id="receivingWalletSelector" aria-label="Floating label select example">
              {transactionCategoryMarkup}
            </select>
            <label htmlFor="receivingWalletSelector">Select transaction category</label>
          </div>
        </div>
      </div>
      <div className="row mt-1 g-2">
        <div className="col-md">
          <div className="form-floating">
            <input type="number" className="form-control" id="amountInput" value={amount} onChange={e => setAmount(e.target.value)} />
            <label htmlFor="amountInput">Amount</label>
          </div>
        </div>
        <div className="col-md">
          <div className="form-floating">
            <input type="text" className="form-control" id="reasonInpit" value={reason} onChange={e => setReason(e.target.value)} disabled={isDisabledReasonField} />
            <label htmlFor="reasonInpit">Reason</label>
          </div>
        </div>
        <div className="col-md">
          <div className="form-floating">
            <select defaultValue={saveAsTemplate} onChange={e => setSaveAsTemplate(e.target.value)} className="form-select" id="saveAsTemplateSelector" aria-label="Floating label select example">
              <option value={`no`}>No</option>
              <option value={`yes`}>Yes</option>
            </select>
            <label htmlFor="saveAsTemplateSelector">Save transaction as a template?</label>
          </div>
        </div>
      </div>
      <div className="row mt-1 g-2">
        <div className="col-md">
          <div className="form-floating">
            <input type="number" className="form-control" id="monthInput" defaultValue={month} onChange={e => setMonth(e.target.value)} min="01" max="12"/>
            <label htmlFor="monthInput">Month</label>
          </div>
        </div>
        <div className="col-md">
          <div className="form-floating">
            <input type="number" className="form-control" id="dayInout" defaultValue={day} onChange={e => setDay(e.target.value)} min="01" max="31"/>
            <label htmlFor="dayInout">Day</label>
          </div>
        </div>
        <div className="col-md">
          <div className="form-floating">
            <input type="number" className="form-control" id="yearInput" defaultValue={year} onChange={e => setYear(e.target.value)} min="2022" max="2032"/>
            <label htmlFor="yearInput">Year</label>
          </div>
        </div>
      </div>
      <div className="row mt-1 g-2">
        <div className="col-md">
          <div className="form-floating">
            <input type="number" className="form-control" id="hourInput" defaultValue={hour} onChange={e => setHour(e.target.value)} min="00" max="23"/>
            <label htmlFor="hourInput">Hour</label>
          </div>
        </div>
        <div className="col-md">
          <div className="form-floating">
            <input type="number" className="form-control" id="minuteInput" defaultValue={minute} onChange={e => setMinute(e.target.value)} min="00" max="59"/>
            <label htmlFor="minuteInput">Minute</label>
          </div>
        </div>
      </div>
      <button onClick={submitHandler} disabled={isDisabledSubmitButton} type="button" className="btn btn-primary m-1">Submit</button>
    </div>
  )
}

export default NewTransactionPage