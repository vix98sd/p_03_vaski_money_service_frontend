import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import apiClient from '../client/api-client'
import ButtonBack from './ButtonBack';
import ButtonDelete from './ButtonDelete';
import LinkButton from './LinkButton';
import TransactionTable from './TransactionTable';

const WalletInfoPage = (props) => {
    const navigate = useNavigate();
    const params = useParams()
    const walletId = params.wallet_id
    const [wallet, setWallet] = useState([])
    const [transactions, setTransactions] = useState([])

    useEffect(() => apiClient.getWalletById(walletId, setWallet, setTransactions), [walletId])

    const deleteHandler = () => {
        if (wallet.balance !== 0) {
            alert("Before deleting balance must be 0!");
        } else {
            apiClient.deleteWalletById(walletId);
            setTimeout(() => navigate('/'), 250)
        }
    }
    const walletInfoMarkup = wallet ? (
        <div>
            <h5 className="card-header">{wallet.name}</h5>
            <ButtonBack />
            <div className="card-body">
                <h2 className="card-title">Balance: <span className="badge bg-danger">{wallet.balance}</span></h2>
                <h3 className="card-title">Category: <span className="badge bg-success">{wallet.category}</span></h3>
                <LinkButton to="/new_transaction" text="Make new transaction" />
                <ButtonDelete onClick={deleteHandler} text="Delete wallet" />
            </div>
            <TransactionTable transactions={transactions} />
        </div>
    ) : null;

    return (
        walletInfoMarkup
    )
}

export default WalletInfoPage