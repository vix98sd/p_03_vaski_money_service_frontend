import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import apiClient from '../client/api-client';
import ButtonBack from './ButtonBack';

const NewWalletPage = () => {
    const navigate = useNavigate();
    const [walletName, setWalletName] = useState('');
    const [isSaving, setIsSaving] = useState(false);

    const submitHandler = () => {
        const wallet = {
            name: walletName,
            category: isSaving ? 'SAVINGS' : 'NORMAL'
        }
        apiClient.postNewWallet(wallet)
        setWalletName('')
        setTimeout(() => navigate('/'), 250)
    };

    return (
        <div>
            <h5 className="card-header">New wallet</h5>
            <ButtonBack />
            <form>
                <div className="mb-3">
                    <label htmlFor="walletName" className="form-label">Wallet name</label>
                    <input value={walletName} onChange={e => setWalletName(e.target.value)} type="text" className="form-control" id="walletName" aria-describedby="emailHelp" />
                </div>
                <div className="mb-3 form-check">
                    <input checked={isSaving} onChange={e => setIsSaving(e.target.checked)} type="checkbox" className="form-check-input" id="isSavingCheck" />
                    <label className="form-check-label" htmlFor="isSavingCheck">Is this wallet for savings</label>
                </div>
                <button disabled={walletName === ''} onClick={submitHandler} type="button" className="btn btn-primary">Submit</button>
            </form>
        </div>
    )
}

export default NewWalletPage