import React from 'react'
import { Link } from 'react-router-dom'
import TableRow from './TableRow'

const WalletTableRow = (props) => {
    const walletMarkup = props.wallet ? (
        <TableRow data={[props.wallet.id, props.wallet.name, props.wallet.category, props.wallet.balance,
            <Link to={`/info/${props.wallet.id}`} className="btn btn-primary">View wallet</Link>]} />) : (
        <TableRow data={['There are no wallets', '/', '/']} />)
    return (
        walletMarkup
    )
}

export default WalletTableRow