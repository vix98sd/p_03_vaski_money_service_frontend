import React, { useEffect, useState } from 'react'
import apiClient from '../client/api-client'
import ButtonInit from './ButtonInit'
import LinkButton from './LinkButton'
import WalletTable from './WalletTable'

const WalletHomePage = () => {
    const [wallets, setWallets] = useState([])
    useEffect(() => apiClient.getAllWallets(setWallets), [])

    const walletsBalanceMarkup = wallets.length > 0 ? (wallets.reduce((balance, wl2) => balance + wl2.balance, 0)) : (0);

    return (
        <div>
            <h5 className="card-header">Your wallets</h5>
            <LinkButton to="/new_wallet" text="Add new wallet" />
            <LinkButton to="/import_export" text="Import / Export data" />
            <ButtonInit setWallets={setWallets}/>
            <h1><span className="badge bg-danger">{walletsBalanceMarkup}</span></h1>
            <WalletTable wallets={wallets}/>
            <LinkButton to='/new_transaction' text="Make new transaction"/>
        </div>
    )
}

export default WalletHomePage