import React from 'react'

const ButtonAction = (props) => {
  return (
    <button onClick={props.onClick} className="btn btn-primary m-1" disabled={props.isDisabled}>{props.text}</button>
  )
}

export default ButtonAction