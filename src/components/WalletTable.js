import React from 'react'
import TableHeadRow from './TableHeadRow'
import WalletTableRow from './WalletTableRow'

const WalletTable = (props) => {
  const wallets = props.wallets ? props.wallets : []
  const walletListMarkup = wallets.length > 0 ? (
    wallets.map(wallet => (<WalletTableRow key={wallet.id} wallet={wallet} />))
  ) : (<WalletTableRow />)

  return (
    <table className="table table-striped table-hover">
      <thead>
        <TableHeadRow data={["Id", "Name", "Category", "Balance"]} />
      </thead>
      <tbody>
          {walletListMarkup}
      </tbody>
    </table>
  )
}

export default WalletTable