import React from 'react'
import apiClient from '../client/api-client';
import ButtonDelete from './ButtonDelete'
import TableRow from './TableRow'

const TransactionTableRow = (props) => {
    const transaction = props.transaction
    const deleteHandler = () => {
        apiClient.deleteTransactionById(transaction.id);
        window.location.reload(false);
    }
    const transactionRomMarkup = props.transaction ? (
        <TableRow data={[
            transaction.id ,
            transaction.amount ,
            transaction.reason ,
            transaction.category ,
            `${transaction.date[0]}/${transaction.date[1]}/${transaction.date[2]}`,
            `${props.transaction.time[0]}:${transaction.time[1]}`,
            transaction.type,
            <ButtonDelete onClick={deleteHandler} text="Delete transaction"/>
        ]} />) : (
        <TableRow data={['There are no transactions', '/', '/', '/', '/', '/', '/']} />)
    return (
        transactionRomMarkup
    )
}

export default TransactionTableRow