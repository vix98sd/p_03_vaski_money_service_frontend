import React, { useEffect, useState } from 'react'
import apiClient from '../client/api-client';
import ButtonAction from './ButtonAction'


const ButtonInit = (props) => {
    const [wallets, setWallets] = useState([])
    const [isDisabled, setIsDisabled] = useState(true)

    useEffect(() => apiClient.getAllWallets(setWallets), [])

    useEffect(() => setIsDisabled(wallets.length > 0), [wallets])

    const submitInitWallets = () => {
        apiClient.initWallets();
        setIsDisabled(true)
        setTimeout(() => apiClient.getAllWallets(props.setWallets), 300)
    }

    return (
        <ButtonAction onClick={submitInitWallets} isDisabled={isDisabled} text={`Init wallets`} />
    )
}

export default ButtonInit