# P_03_VASki_Money_service_frontend

Money service - frontend for VA Ski project.

VA Ski - Virtual Assistant Ski

## Getting started

### How to start application locally:
- [ ] Make sure that you have [money service backend](https://gitlab.com/vix98sd/p_03_vaski_money_service) running
- [ ] To start money service frontend run following commands:
- [ ] `git clone https://gitlab.com/vix98sd/p_03_vaski_money_service_frontend.git` 
- [ ] `npm start`

### How to start application in kubernetes:
- [ ] Make sure that you have [money service backend](https://gitlab.com/vix98sd/p_03_vaski_money_service) running
- [ ] Download/Clone files from [kubernetes](https://gitlab.com/vix98sd/p_03_vaski_money_service_frontend/-/tree/development/kubernetes) directory
- [ ] Change working directory to `./kubernetes` directory and apply following command: `kubectl apply -f .`
- [ ] To try money service frontend go to the [localhost:30102](http://localhost:30102)
- [ ] This system is intended to work on a single-node cluster, if you run it on a multi-node cluster some errors may occur